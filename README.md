# de_dust2

## Smokes

### Smoke CT slope / deep CT from Long doors

![long_doors_deep_ct_01.jpg](img/long_doors_deep_ct_01.jpg)

![long_doors_deep_ct_02.jpg](img/long_doors_deep_ct_02.jpg)

![long_doors_deep_ct_03.jpg](img/long_doors_deep_ct_03.jpg)

![long_doors_deep_ct_04_result.jpg](img/long_doors_deep_ct_04_result.jpg)

### Smoke CT mid corner from lower

Good if theres someone peeking xbox

![lower_to_b_slope_corner_01.jpg](img/lower_to_b_slope_corner_01.jpg)

![lower_to_b_slope_corner_02.jpg](img/lower_to_b_slope_corner_02.jpg)

![lower_to_b_slope_corner_03_result.jpg](img/lower_to_b_slope_corner_03_result.jpg)

### Lower dark xbox smoke

![lower_to_xbox_01.jpg](img/lower_to_xbox_01.jpg)

![lower_to_xbox_02_result.jpg](img/lower_to_xbox_02_result.jpg)

### Mid-to-B CT smoke

![mid_to_b_ct_smoke_01.jpg](img/mid_to_b_ct_smoke_01.jpg)

![mid_to_b_ct_smoke_02.jpg](img/mid_to_b_ct_smoke_02.jpg)

![mid_to_b_ct_smoke_03.jpg](img/mid_to_b_ct_smoke_03.jpg)

### Mid-to-B CT smoke from Long (good fake)

Good for fakes

![mid_to_b_ct_smoke_from_long_01.jpg](img/mid_to_b_ct_smoke_from_long_01.jpg)

![mid_to_b_ct_smoke_from_long_02.jpg](img/mid_to_b_ct_smoke_from_long_02.jpg)

![mid_to_b_ct_smoke_from_long_03_result.jpg](img/mid_to_b_ct_smoke_from_long_03_result.jpg)

### From pit to A site

Good if you're stuck in pit and someone is peeking you with an awp

![pit_to_a_site_01.jpg](img/pit_to_a_site_01.jpg)

![pit_to_a_site_02.jpg](img/pit_to_a_site_02.jpg)

![pit_to_a_site_03.jpg](img/pit_to_a_site_03.jpg)

### Smoke CT spawn cross from short

![short_to_ct_smoke_01.jpg](img/short_to_ct_smoke_01.jpg)

![short_to_ct_smoke_02.jpg](img/short_to_ct_smoke_02.jpg)

![short_to_ct_smoke_03_result.jpg](img/short_to_ct_smoke_03_result.jpg)

### Smoke long car from T spawn

![t_spawn_long_car_01.jpg](img/t_spawn_long_car_01.jpg)

![t_spawn_long_car_02.jpg](img/t_spawn_long_car_02.jpg)

![t_spawn_long_car_03_result.jpg](img/t_spawn_long_car_03_result.jpg)

### Smoke long corner from T spawn

![t_spawn_long_corner_01.jpg](img/t_spawn_long_corner_01.jpg)

![t_spawn_long_corner_02.jpg](img/t_spawn_long_corner_02.jpg)

![t_spawn_long_corner_03.jpg](img/t_spawn_long_corner_03.jpg)

### Smoke CT spawn long cross from top mid

Good for fakes or something

![top_mid_deep_ct_long_01.jpg](img/top_mid_deep_ct_long_01.jpg)

![top_mid_deep_ct_long_02.jpg](img/top_mid_deep_ct_long_02.jpg)

![top_mid_deep_ct_long_03.jpg](img/top_mid_deep_ct_long_03.jpg)

### Smoke xbox from T spawn

![tspawn_to_xbox_01.jpg](img/tspawn_to_xbox_01.jpg)

![tspawn_to_xbox_02.jpg](img/tspawn_to_xbox_02.jpg)

![tspawn_to_xbox_03_result.jpg](img/tspawn_to_xbox_03_result.jpg)

### Smoke off portion of B site from tunnel

Good if you know someone is peeking tunnel from inside B site

![tunnel_to_b_smoke_01.jpg](img/tunnel_to_b_smoke_01.jpg)

![tunnel_to_b_smoke_02.jpg](img/tunnel_to_b_smoke_02.jpg)

![tunnel_to_b_smoke_03_result.jpg](img/tunnel_to_b_smoke_03_result.jpg)

### Xbox smoke from outside long

![xbox_smoke_from_long_area_01.jpg](img/xbox_smoke_from_long_area_01.jpg)

![xbox_smoke_from_long_area_02.jpg](img/xbox_smoke_from_long_area_02.jpg)

![xbox_smoke_from_long_area_03.jpg](img/xbox_smoke_from_long_area_03.jpg)

## Flashes

### One way flash for short


![flash_short_oneway_01.jpg](img/flash_short_oneway_01.jpg)

![flash_short_oneway_02.jpg](img/flash_short_oneway_02.jpg)
